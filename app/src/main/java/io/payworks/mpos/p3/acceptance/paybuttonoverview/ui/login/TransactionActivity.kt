package io.payworks.mpos.p3.acceptance.paybuttonoverview.ui.login

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import io.mpos.accessories.AccessoryFamily
import io.mpos.accessories.AccessoryFamily.*
import io.mpos.accessories.parameters.AccessoryParameters
import io.mpos.provider.ProviderMode
import io.mpos.transactionprovider.processparameters.TransactionProcessParameters
import io.mpos.transactionprovider.processparameters.steps.tipping.TippingProcessStepParameters
import io.mpos.transactions.Currency
import io.mpos.transactions.Transaction
import io.mpos.transactions.TransactionWorkflowType
import io.mpos.transactions.parameters.TransactionParameters
import io.mpos.ui.shared.MposUi
import io.mpos.ui.shared.model.MposUiConfiguration
import io.mpos.ui.shared.model.MposUiConfiguration.PaymentOption.*
import io.mpos.ui.shared.model.MposUiConfiguration.SummaryFeature.*
import io.payworks.mpos.p3.acceptance.paybuttonoverview.R
import java.math.BigDecimal
import java.util.*


class TransactionActivity : AppCompatActivity() {

    var accessoryParameters: AccessoryParameters = accessoryParameters(MIURA_MPI)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_transaction)

        findViewById<Button>(R.id.startTransaction).setOnClickListener {
            val amount = findViewById<EditText>(R.id.amount)
            startTransaction(amount = BigDecimal(amount.text.toString()))
        }

        findViewById<Button>(R.id.tipAdjust).setOnClickListener { tipAdjust() }
        findViewById<Button>(R.id.linkedRefund).setOnClickListener { linkedRefund() }
        findViewById<Button>(R.id.transactionLookup).setOnClickListener { transactionLookup() }


    }
    private fun accessoryParameters(family: AccessoryFamily) = when(family) {
        VERIFONE_VIPA -> AccessoryParameters.Builder(VERIFONE_VIPA)
            .tcp("192.168.0.111", 8090)
            .build()
        PAX -> AccessoryParameters.Builder(PAX)
            .integrated()
            .build()
        else -> AccessoryParameters.Builder(MIURA_MPI)
            .bluetooth()
            .build()
    }

    private fun startTransaction(amount: BigDecimal) {
        // Gateway Credentials

        // acquirer mode
        val ui = MposUi.initialize(
            this,
            ProviderMode.TEST,
            "cebae7f2-60c5-4264-891d-5a4f56018c06",
            "I0102ooET58XtXFa6aZAV9MNzTEmZFb1"
        )

//        // provider mode
//        val ui = MposUi.initialize(this, ProviderMode.TEST, ApplicationName.BARCLAYCARD, "JulianoAcceptBarclaycard")

        // Accessory Parameters
        ui.configuration.terminalParameters = accessoryParameters

        // Transaction Parameters
        val transactionParameters = TransactionParameters.Builder()
            .charge(amount, if (accessoryParameters.accessoryFamily == VERIFONE_VIPA) Currency.GBP else Currency.EUR)
            //.workflow(TransactionWorkflowType.MOTO)
            .subject("Bouquet of Flowers")
            .customIdentifier("my-product-id-12345")
            .metadata(
                mapOf(Pair("clerk", "Juliano Rotta"), Pair("commission", "2%"))
            )
            .build()

        // Tipping

        val steps = TippingProcessStepParameters.Builder()
            .askForTipAmount()
            .showAddTipConfirmationScreen(true) //new way of enabling add tip screen
            .showTotalAmountConfirmationScreen(true) //enable the total amount confirmation screen
            .maxTipAmount(BigDecimal(5.0)) // enables the maximum acceptable tip amount check
            .numberFormat(6, 2).build()

        val transactionProcessParameters =
            TransactionProcessParameters.Builder()
                .addStep(steps)
                .build()


        // Customization

        // Card and Alipay transactions
        ui.configuration.paymentOptions = EnumSet.of(CARD)
       // ui.configuration.paymentOptions = EnumSet.of(CARD, ALIPAY, WECHAT_PAY)


        ui.configuration.appearance.apply {
            colorPrimary = Color.MAGENTA
            colorPrimaryDark = Color.DKGRAY
            textColorPrimary = Color.WHITE

            approvedBackgroundColor = Color.BLACK
            declinedBackgroundColor = Color.RED
            refundedBackgroundColor = Color.YELLOW
        }

        ui.configuration.summaryFeatures = EnumSet.of(
            REFUND_TRANSACTION,
            SEND_RECEIPT_VIA_EMAIL,
            PRINT_CUSTOMER_RECEIPT,
            PRINT_MERCHANT_RECEIPT,
            CAPTURE_TRANSACTION
        )

        ui.configuration.printerParameters = accessoryParameters(PAX)
        ui.configuration.signatureCapture = MposUiConfiguration.SignatureCapture.ON_RECEIPT

        val intent = ui.createTransactionIntent(transactionParameters)
        startActivityForResult(intent, MposUi.REQUEST_CODE_PAYMENT)
    }

     private fun tipAdjust() {
        val ui = MposUi.initialize(this, ProviderMode.MOCK, "cebae7f2-60c5-4264-891d-5a4f56018c06", "I0102ooET58XtXFa6aZAV9MNzTEmZFb1")
        val transactionParameters = TransactionParameters.Builder()
            .adjustTip("transaction-id-10-usd", BigDecimal("2.00"), Currency.USD)
            .build()

        val intent = ui.createTransactionIntent(transactionParameters)
        startActivityForResult(intent, MposUi.REQUEST_CODE_PAYMENT)
    }

    private fun linkedRefund() {
        val ui = MposUi.initialize(this, ProviderMode.MOCK, "cebae7f2-60c5-4264-891d-5a4f56018c06", "I0102ooET58XtXFa6aZAV9MNzTEmZFb1")
        val parameters = TransactionParameters.Builder()
            .refund("<transactionIdentifer>")
            .build()
        val intent: Intent = ui.createTransactionIntent(parameters)
        startActivityForResult(intent, MposUi.REQUEST_CODE_PAYMENT)
    }

    private fun transactionLookup() {
        val ui = MposUi.initialize(this, ProviderMode.TEST, "12a0ea93-850c-45e6-af65-9aa30bf7ad70", "Bq9tC2IFAqYtCeu0zQSJ13JhK37jc2N0")
        ui.configuration.summaryFeatures = EnumSet.of(
                REFUND_TRANSACTION,
                SEND_RECEIPT_VIA_EMAIL,
                PRINT_CUSTOMER_RECEIPT,
                PRINT_MERCHANT_RECEIPT,
                CAPTURE_TRANSACTION
            )

        ui.configuration.terminalParameters = accessoryParameters
        ui.configuration.printerParameters = accessoryParameters

        val intent: Intent = ui.createTransactionSummaryIntent("fdc77f49dc6c11eab0c1bdd980166002")
        startActivityForResult(intent, MposUi.REQUEST_CODE_SHOW_SUMMARY)
    }

    fun onRadioButtonClicked(view: View) {
        if (view is RadioButton) {
            // Is the button now checked?
            val checked = view.isChecked

            // Check which radio button was clicked
            when (view.getId()) {
                R.id.vipa ->
                    if (checked) {
                        accessoryParameters = accessoryParameters(VERIFONE_VIPA)
                    }
                R.id.pax ->
                    if (checked) {
                        accessoryParameters = accessoryParameters(PAX)
                    }
                R.id.miura ->
                    if (checked) {
                        accessoryParameters = accessoryParameters(MIURA_MPI)
                    }
            }
        }
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == MposUi.REQUEST_CODE_PAYMENT) {
            if (resultCode == MposUi.RESULT_CODE_APPROVED) {
                // Transaction was approved
                val transaction: Transaction = MposUi.getInitializedInstance().transaction
                Toast.makeText(
                    this,
                    "Transaction approved\n ${transaction.details.metadata}\n${transaction.customIdentifier}",
                    Toast.LENGTH_LONG
                ).show()
            } else {
                // Card was declined, or transaction was aborted, or failed
                // (e.g. no internet or accessory not found)
                Toast.makeText(
                    this,
                    "Transaction was declined, aborted, or failed",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }
}